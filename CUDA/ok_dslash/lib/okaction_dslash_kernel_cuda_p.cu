#include "xxx_cuda_internal.h"

__device__ __constant__ GLA_Real c_kap;

//=========================================================================
// Color matrix multiplication modules with color vector
//
//=========================================================================
__device__ void GLA_V_peq_M_times_V( GLA_ColorVector * __restrict__ y, 
                                     GLA_ColorMatrix * __restrict__ M, 
                                     GLA_ColorVector * __restrict__ x )
{
  GLA_Real xc0Re = x->c0Re;
  GLA_Real xc0Im = x->c0Im;
  GLA_Real xc1Re = x->c1Re;
  GLA_Real xc1Im = x->c1Im;
  GLA_Real xc2Re = x->c2Re;
  GLA_Real xc2Im = x->c2Im;

  y->c0Re += (  M->c00Re * xc0Re - M->c00Im * xc0Im
              + M->c01Re * xc1Re - M->c01Im * xc1Im
              + M->c02Re * xc2Re - M->c02Im * xc2Im );
  
  y->c0Im += (  M->c00Re * xc0Im + M->c00Im * xc0Re
              + M->c01Re * xc1Im + M->c01Im * xc1Re
              + M->c02Re * xc2Im + M->c02Im * xc2Re );

  y->c1Re += (  M->c10Re * xc0Re - M->c10Im * xc0Im
              + M->c11Re * xc1Re - M->c11Im * xc1Im
              + M->c12Re * xc2Re - M->c12Im * xc2Im );
  
  y->c1Im += (  M->c10Re * xc0Im + M->c10Im * xc0Re
              + M->c11Re * xc1Im + M->c11Im * xc1Re
              + M->c12Re * xc2Im + M->c12Im * xc2Re );

  y->c2Re += (  M->c20Re * xc0Re - M->c20Im * xc0Im
              + M->c21Re * xc1Re - M->c21Im * xc1Im
              + M->c22Re * xc2Re - M->c22Im * xc2Im );
  
  y->c2Im += (  M->c20Re * xc0Im + M->c20Im * xc0Re
              + M->c21Re * xc1Im + M->c21Im * xc1Re
              + M->c22Re * xc2Im + M->c22Im * xc2Re );
}

__device__ void GLA_V_meq_M_times_V( GLA_ColorVector * __restrict__ y, 
                                     GLA_ColorMatrix * __restrict__ M, 
                                     GLA_ColorVector * __restrict__ x )
{
  GLA_Real xc0Re = x->c0Re;
  GLA_Real xc0Im = x->c0Im;
  GLA_Real xc1Re = x->c1Re;
  GLA_Real xc1Im = x->c1Im;
  GLA_Real xc2Re = x->c2Re;
  GLA_Real xc2Im = x->c2Im;

  y->c0Re -= (  M->c00Re * xc0Re - M->c00Im * xc0Im
              + M->c01Re * xc1Re - M->c01Im * xc1Im
              + M->c02Re * xc2Re - M->c02Im * xc2Im );
  
  y->c0Im -= (  M->c00Re * xc0Im + M->c00Im * xc0Re
              + M->c01Re * xc1Im + M->c01Im * xc1Re
              + M->c02Re * xc2Im + M->c02Im * xc2Re );

  y->c1Re -= (  M->c10Re * xc0Re - M->c10Im * xc0Im
              + M->c11Re * xc1Re - M->c11Im * xc1Im
              + M->c12Re * xc2Re - M->c12Im * xc2Im );
  
  y->c1Im -= (  M->c10Re * xc0Im + M->c10Im * xc0Re
              + M->c11Re * xc1Im + M->c11Im * xc1Re
              + M->c12Re * xc2Im + M->c12Im * xc2Re );

  y->c2Re -= (  M->c20Re * xc0Re - M->c20Im * xc0Im
              + M->c21Re * xc1Re - M->c21Im * xc1Im
              + M->c22Re * xc2Re - M->c22Im * xc2Im );
  
  y->c2Im -= (  M->c20Re * xc0Im + M->c20Im * xc0Re
              + M->c21Re * xc1Im + M->c21Im * xc1Re
              + M->c22Re * xc2Im + M->c22Im * xc2Re );
}

__device__ void GLA_V_peq_Ma_times_V( GLA_ColorVector * __restrict__ y, 
                                      GLA_ColorMatrix * __restrict__ M, 
                                      GLA_ColorVector * __restrict__ x )
{
  GLA_Real xc0Re = x->c0Re;
  GLA_Real xc0Im = x->c0Im;
  GLA_Real xc1Re = x->c1Re;
  GLA_Real xc1Im = x->c1Im;
  GLA_Real xc2Re = x->c2Re;
  GLA_Real xc2Im = x->c2Im;

  y->c0Re += (  M->c00Re * xc0Re + M->c00Im * xc0Im
              + M->c10Re * xc1Re + M->c10Im * xc1Im
              + M->c20Re * xc2Re + M->c20Im * xc2Im );
  
  y->c0Im += (  M->c00Re * xc0Im - M->c00Im * xc0Re
              + M->c10Re * xc1Im - M->c10Im * xc1Re
              + M->c20Re * xc2Im - M->c20Im * xc2Re );

  y->c1Re += (  M->c01Re * xc0Re + M->c01Im * xc0Im
              + M->c11Re * xc1Re + M->c11Im * xc1Im
              + M->c21Re * xc2Re + M->c21Im * xc2Im );
  
  y->c1Im += (  M->c01Re * xc0Im - M->c01Im * xc0Re
              + M->c11Re * xc1Im - M->c11Im * xc1Re
              + M->c21Re * xc2Im - M->c21Im * xc2Re );

  y->c2Re += (  M->c02Re * xc0Re + M->c02Im * xc0Im
              + M->c12Re * xc1Re + M->c12Im * xc1Im
              + M->c22Re * xc2Re + M->c22Im * xc2Im );
  
  y->c2Im += (  M->c02Re * xc0Im - M->c02Im * xc0Re
              + M->c12Re * xc1Im - M->c12Im * xc1Re
              + M->c22Re * xc2Im - M->c22Im * xc2Re );
}

__device__ void GLA_V_meq_Ma_times_V( GLA_ColorVector * __restrict__ y, 
                                      GLA_ColorMatrix * __restrict__ M, 
                                      GLA_ColorVector * __restrict__ x )
{
  GLA_Real xc0Re = x->c0Re;
  GLA_Real xc0Im = x->c0Im;
  GLA_Real xc1Re = x->c1Re;
  GLA_Real xc1Im = x->c1Im;
  GLA_Real xc2Re = x->c2Re;
  GLA_Real xc2Im = x->c2Im;

  y->c0Re -= (  M->c00Re * xc0Re + M->c00Im * xc0Im
              + M->c10Re * xc1Re + M->c10Im * xc1Im
              + M->c20Re * xc2Re + M->c20Im * xc2Im );
  
  y->c0Im -= (  M->c00Re * xc0Im - M->c00Im * xc0Re
              + M->c10Re * xc1Im - M->c10Im * xc1Re
              + M->c20Re * xc2Im - M->c20Im * xc2Re );

  y->c1Re -= (  M->c01Re * xc0Re + M->c01Im * xc0Im
              + M->c11Re * xc1Re + M->c11Im * xc1Im
              + M->c21Re * xc2Re + M->c21Im * xc2Im );
  
  y->c1Im -= (  M->c01Re * xc0Im - M->c01Im * xc0Re
              + M->c11Re * xc1Im - M->c11Im * xc1Re
              + M->c21Re * xc2Im - M->c21Im * xc2Re );

  y->c2Re -= (  M->c02Re * xc0Re + M->c02Im * xc0Im
              + M->c12Re * xc1Re + M->c12Im * xc1Im
              + M->c22Re * xc2Re + M->c22Im * xc2Im );
  
  y->c2Im -= (  M->c02Re * xc0Im - M->c02Im * xc0Re
              + M->c12Re * xc1Im - M->c12Im * xc1Re
              + M->c22Re * xc2Im - M->c22Im * xc2Re );
}

//=========================================================================
// Kernel for 0-hop term 
//
//=========================================================================
__global__ void OnSite
( GLA_Real *d_ans_s0, GLA_Real *d_ans_s1, 
  GLA_Real *d_ans_s2, GLA_Real *d_ans_s3, 
  GLA_Real *d_D0_b0, GLA_Real *d_D0_b1, 
  GLA_Real *d_S0_b0, GLA_Real *d_S0_b1,
  GLA_Real *d_src_s0, GLA_Real *d_src_s1, 
  GLA_Real *d_src_s2, GLA_Real *d_src_s3 )
{

  //=================
  // Local Variables
  //=================
  //int tpb = blockDim.x;   // number of threads per block
  int tx = threadIdx.x;
  int sitesPerBlock = THREAD*blockIdx.x;
  int siteV = 6*sitesPerBlock + tx;
  int siteM = 18*sitesPerBlock + tx;
  int idx;

  GLA_ColorVector ans_s0;
  GLA_ColorVector ans_s1;
  GLA_ColorVector ans_s2;
  GLA_ColorVector ans_s3;
  GLA_ColorVector src_s0;
  GLA_ColorVector src_s1;
  GLA_ColorVector src_s2;
  GLA_ColorVector src_s3;
  GLA_ColorMatrix D0_b0;
  GLA_ColorMatrix D0_b1;
  GLA_ColorMatrix S0_b0;
  GLA_ColorMatrix S0_b1;
  
  //GLA_ColorVector zeroV = { .c0Re = 0., .c0Im = 0.,
  //                          .c1Re = 0., .c1Im = 0.,
  //                          .c2Re = 0., .c2Im = 0. };

  __shared__ GLA_Real s_tmp[THREAD*18];

  //================================================ 
  // Read QLA_ColorMatrix Through the Shared Memory
  //================================================ 
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_D0_b0[siteM+THREAD*idx];
  }
  __syncthreads();

    D0_b0 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_D0_b1[siteM+THREAD*idx];
  }
  __syncthreads();

    D0_b1 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_S0_b0[siteM+THREAD*idx];
  }
  __syncthreads();

    S0_b0 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_S0_b1[siteM+THREAD*idx];
  }
  __syncthreads();

    S0_b1 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  
  //======================================================= 
  // Read Source QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s3 = ((GLA_ColorVector *)s_tmp)[tx];
//  __syncthreads();
  
  //============ 
  // Initialize
  //============ 
  //ans_s0 = zeroV;
  //ans_s1 = zeroV;
  //ans_s2 = zeroV;
  //ans_s3 = zeroV;
  ans_s0.c0Re = c_kap*src_s0.c0Re;
  ans_s0.c0Im = c_kap*src_s0.c0Im;
  ans_s0.c1Re = c_kap*src_s0.c1Re;
  ans_s0.c1Im = c_kap*src_s0.c1Im;
  ans_s0.c2Re = c_kap*src_s0.c2Re;
  ans_s0.c2Im = c_kap*src_s0.c2Im;
  
  ans_s1.c0Re = c_kap*src_s1.c0Re;
  ans_s1.c0Im = c_kap*src_s1.c0Im;
  ans_s1.c1Re = c_kap*src_s1.c1Re;
  ans_s1.c1Im = c_kap*src_s1.c1Im;
  ans_s1.c2Re = c_kap*src_s1.c2Re;
  ans_s1.c2Im = c_kap*src_s1.c2Im;
  
  ans_s2.c0Re = c_kap*src_s2.c0Re;
  ans_s2.c0Im = c_kap*src_s2.c0Im;
  ans_s2.c1Re = c_kap*src_s2.c1Re;
  ans_s2.c1Im = c_kap*src_s2.c1Im;
  ans_s2.c2Re = c_kap*src_s2.c2Re;
  ans_s2.c2Im = c_kap*src_s2.c2Im;
  
  ans_s3.c0Re = c_kap*src_s3.c0Re;
  ans_s3.c0Im = c_kap*src_s3.c0Im;
  ans_s3.c1Re = c_kap*src_s3.c1Re;
  ans_s3.c1Im = c_kap*src_s3.c1Im;
  ans_s3.c2Re = c_kap*src_s3.c2Re;
  ans_s3.c2Im = c_kap*src_s3.c2Im;
  
  //=======
  // Dlash
  //=======
  GLA_V_peq_M_times_V(&ans_s0,&D0_b0,&src_s0); 
  GLA_V_peq_M_times_V(&ans_s0,&D0_b1,&src_s1); 
  GLA_V_peq_M_times_V(&ans_s0,&S0_b0,&src_s2); 
  GLA_V_peq_M_times_V(&ans_s0,&S0_b1,&src_s3);

  GLA_V_peq_Ma_times_V(&ans_s1,&D0_b1,&src_s0);
  GLA_V_meq_Ma_times_V(&ans_s1,&D0_b0,&src_s1);
  GLA_V_meq_Ma_times_V(&ans_s1,&S0_b1,&src_s2);
  GLA_V_peq_Ma_times_V(&ans_s1,&S0_b0,&src_s3);
  
  GLA_V_meq_M_times_V(&ans_s2,&S0_b0,&src_s0); 
  GLA_V_meq_M_times_V(&ans_s2,&S0_b1,&src_s1); 
  GLA_V_peq_M_times_V(&ans_s2,&D0_b0,&src_s2); 
  GLA_V_peq_M_times_V(&ans_s2,&D0_b1,&src_s3);

  GLA_V_peq_Ma_times_V(&ans_s3,&S0_b1,&src_s0);
  GLA_V_meq_Ma_times_V(&ans_s3,&S0_b0,&src_s1);
  GLA_V_peq_Ma_times_V(&ans_s3,&D0_b1,&src_s2);
  GLA_V_meq_Ma_times_V(&ans_s3,&D0_b0,&src_s3);

//  __syncthreads();

  //======================================================= 
  // Write Result QLA_ColorVector Through the Shared Memory
  //======================================================= 
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s0;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s0[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s1;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s1[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s2;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s2[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s3;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s3[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
//  __syncthreads();

//  for( ic = 0; ic < 6; ic++ ){
//    offset = ic*THREAD + siteV;
//    ans_s0[offset] = ic;
//    ans_s1[offset] = ic+6;
//    ans_s2[offset] = ic+12;
//    ans_s3[offset] = ic+18;
//  }

}

//=========================================================================
// Kernel for 1-hop term in time direction 
//
//=========================================================================
__global__ void OneHopTime
( GLA_Real *d_ans_s0, GLA_Real *d_ans_s1, 
  GLA_Real *d_ans_s2, GLA_Real *d_ans_s3, 
  GLA_Real *d_pull_s0, GLA_Real *d_pull_s1, 
  GLA_Real *d_pull_s2, GLA_Real *d_pull_s3, 
  GLA_Real *d_Sp3_b0, GLA_Real *d_Sp3_b1, 
  GLA_Real *d_Sp3_b2, GLA_Real *d_Sp3_b3, GLA_Real *d_U3,
  GLA_Real *d_fwd_s0, GLA_Real *d_fwd_s1, 
  GLA_Real *d_fwd_s2, GLA_Real *d_fwd_s3, 
  GLA_Real *d_src_s0, GLA_Real *d_src_s1, 
  GLA_Real *d_src_s2, GLA_Real *d_src_s3 )
{

  //=================
  // Local Variables
  //=================
  //int tpb = blockDim.x;   // number of threads per block
  int tx = threadIdx.x;
  int sitesPerBlock = THREAD*blockIdx.x;
  int siteV = 6*sitesPerBlock + tx;
  int siteM = 18*sitesPerBlock + tx;
  int idx;

  GLA_ColorVector ans_s0, ans_s1, ans_s2, ans_s3;
  GLA_ColorVector pull_s0, pull_s1, pull_s2, pull_s3;
  GLA_ColorVector fwd_s0, fwd_s1, fwd_s2, fwd_s3;
  GLA_ColorVector src_s0, src_s1, src_s2, src_s3;
  GLA_ColorMatrix Sp3_b0, Sp3_b1, Sp3_b2, Sp3_b3;
  GLA_ColorMatrix U3;
  
  GLA_ColorVector zeroV = { .c0Re = 0., .c0Im = 0.,
                            .c1Re = 0., .c1Im = 0.,
                            .c2Re = 0., .c2Im = 0. };

  __shared__ GLA_Real s_tmp[THREAD*18];

  //================================================ 
  // Read QLA_ColorMatrix Through the Shared Memory
  //================================================ 
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Sp3_b0[siteM+THREAD*idx];
  }
  __syncthreads();

    Sp3_b0 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Sp3_b1[siteM+THREAD*idx];
  }
  __syncthreads();

    Sp3_b1 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Sp3_b2[siteM+THREAD*idx];
  }
  __syncthreads();

    Sp3_b2 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Sp3_b3[siteM+THREAD*idx];
  }
  __syncthreads();

    Sp3_b3 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_U3[siteM+THREAD*idx];
  }
  __syncthreads();

    U3 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  
  //======================================================= 
  // Read Source QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s3 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s3 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();

  //======================================================= 
  // Read Result QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_ans_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    ans_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_ans_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    ans_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_ans_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    ans_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_ans_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    ans_s3 = ((GLA_ColorVector *)s_tmp)[tx];
//  __syncthreads();

  //============ 
  // Initialize
  //============ 
  pull_s0 = zeroV;
  pull_s1 = zeroV;
  pull_s2 = zeroV;
  pull_s3 = zeroV;
  
  //========
  // Dslash
  //========
  // +3
  GLA_V_peq_M_times_V(&ans_s0,&Sp3_b0,&fwd_s2);
  GLA_V_peq_M_times_V(&ans_s0,&Sp3_b1,&fwd_s3);
  
  GLA_V_peq_M_times_V(&ans_s1,&Sp3_b2,&fwd_s2);
  GLA_V_peq_M_times_V(&ans_s1,&Sp3_b3,&fwd_s3);
  
  GLA_V_peq_M_times_V(&ans_s2,&Sp3_b0,&fwd_s0);
  GLA_V_peq_M_times_V(&ans_s2,&Sp3_b1,&fwd_s1);
  GLA_V_meq_M_times_V(&ans_s2,&U3    ,&fwd_s2);

  GLA_V_peq_M_times_V(&ans_s3,&Sp3_b2,&fwd_s0);
  GLA_V_peq_M_times_V(&ans_s3,&Sp3_b3,&fwd_s1);
  GLA_V_meq_M_times_V(&ans_s3,&U3    ,&fwd_s3);
  
  //-3
  GLA_V_meq_Ma_times_V(&pull_s0,&U3    ,&src_s0);
  GLA_V_meq_Ma_times_V(&pull_s0,&Sp3_b0,&src_s2);
  GLA_V_meq_Ma_times_V(&pull_s0,&Sp3_b2,&src_s3);
  
  GLA_V_meq_Ma_times_V(&pull_s1,&U3    ,&src_s1);
  GLA_V_meq_Ma_times_V(&pull_s1,&Sp3_b1,&src_s2);
  GLA_V_meq_Ma_times_V(&pull_s1,&Sp3_b3,&src_s3);
  
  GLA_V_meq_Ma_times_V(&pull_s2,&Sp3_b0,&src_s0);
  GLA_V_meq_Ma_times_V(&pull_s2,&Sp3_b2,&src_s1);

  GLA_V_meq_Ma_times_V(&pull_s3,&Sp3_b1,&src_s0);
  GLA_V_meq_Ma_times_V(&pull_s3,&Sp3_b3,&src_s1);
//  __syncthreads();

  //======================================================= 
  // Write Result QLA_ColorVector Through the Shared Memory
  //======================================================= 
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s0;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s0[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s1;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s1[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s2;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s2[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s3;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s3[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s0;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s0[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s1;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s1[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s2;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s2[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s3;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s3[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
//  __syncthreads();
}

//=========================================================================
// Kernel for 1-hop term in space direction 
//
//=========================================================================
__global__ void OneHopSpace
( GLA_Real *d_ans_s0, GLA_Real *d_ans_s1, 
  GLA_Real *d_ans_s2, GLA_Real *d_ans_s3, 
  GLA_Real *d_pull_s0, GLA_Real *d_pull_s1, 
  GLA_Real *d_pull_s2, GLA_Real *d_pull_s3, 
  GLA_Real *d_Dp_b0, GLA_Real *d_Dp_b1, 
  GLA_Real *d_Dp_b2, GLA_Real *d_Dp_b3,
  GLA_Real *d_Sp_b0, GLA_Real *d_Sp_b1, 
  GLA_Real *d_Sp_b2, GLA_Real *d_Sp_b3,
  GLA_Real *d_fwd_s0, GLA_Real *d_fwd_s1, 
  GLA_Real *d_fwd_s2, GLA_Real *d_fwd_s3, 
  GLA_Real *d_src_s0, GLA_Real *d_src_s1, 
  GLA_Real *d_src_s2, GLA_Real *d_src_s3 )
{

  //=================
  // Local Variables
  //=================
  //int tpb = blockDim.x;   // number of threads per block
  int tx = threadIdx.x;
  int sitesPerBlock = THREAD*blockIdx.x;
  int siteV = 6*sitesPerBlock + tx;
  int siteM = 18*sitesPerBlock + tx;
  int idx;

  GLA_ColorVector ans_s0, ans_s1, ans_s2, ans_s3;
  GLA_ColorVector pull_s0, pull_s1, pull_s2, pull_s3;
  GLA_ColorVector fwd_s0, fwd_s1, fwd_s2, fwd_s3;
  GLA_ColorVector src_s0, src_s1, src_s2, src_s3;
  GLA_ColorMatrix Dp_b0, Dp_b1, Dp_b2, Dp_b3;
  GLA_ColorMatrix Sp_b0, Sp_b1, Sp_b2, Sp_b3;
  
  GLA_ColorVector zeroV = { .c0Re = 0., .c0Im = 0.,
                            .c1Re = 0., .c1Im = 0.,
                            .c2Re = 0., .c2Im = 0. };

  __shared__ GLA_Real s_tmp[THREAD*18];

  //================================================ 
  // Read QLA_ColorMatrix Through the Shared Memory
  //================================================ 
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Dp_b0[siteM+THREAD*idx];
  }
  __syncthreads();

    Dp_b0 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Dp_b1[siteM+THREAD*idx];
  }
  __syncthreads();

    Dp_b1 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Dp_b2[siteM+THREAD*idx];
  }
  __syncthreads();

    Dp_b2 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Dp_b3[siteM+THREAD*idx];
  }
  __syncthreads();

    Dp_b3 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Sp_b0[siteM+THREAD*idx];
  }
  __syncthreads();

    Sp_b0 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Sp_b1[siteM+THREAD*idx];
  }
  __syncthreads();

    Sp_b1 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Sp_b2[siteM+THREAD*idx];
  }
  __syncthreads();

    Sp_b2 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Sp_b3[siteM+THREAD*idx];
  }
  __syncthreads();

    Sp_b3 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  
  //======================================================= 
  // Read Source QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_src_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    src_s3 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s3 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();

  //======================================================= 
  // Read Result QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_ans_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    ans_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_ans_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    ans_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_ans_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    ans_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_ans_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    ans_s3 = ((GLA_ColorVector *)s_tmp)[tx];
//  __syncthreads();

  //============ 
  // Initialize
  //============ 
  pull_s0 = zeroV;
  pull_s1 = zeroV;
  pull_s2 = zeroV;
  pull_s3 = zeroV;
  
  //========
  // Dslash
  //========
  // +i ( i = 0,1,2 )
  GLA_V_peq_M_times_V(&ans_s0,&Dp_b0,&fwd_s0);
  GLA_V_peq_M_times_V(&ans_s0,&Dp_b1,&fwd_s1);
  GLA_V_peq_M_times_V(&ans_s0,&Sp_b0,&fwd_s2);
  GLA_V_peq_M_times_V(&ans_s0,&Sp_b1,&fwd_s3);
  
  GLA_V_peq_M_times_V(&ans_s1,&Dp_b2,&fwd_s0);
  GLA_V_peq_M_times_V(&ans_s1,&Dp_b3,&fwd_s1);
  GLA_V_peq_M_times_V(&ans_s1,&Sp_b2,&fwd_s2);
  GLA_V_peq_M_times_V(&ans_s1,&Sp_b3,&fwd_s3);
  
  GLA_V_peq_M_times_V(&ans_s2,&Sp_b0,&fwd_s0);
  GLA_V_peq_M_times_V(&ans_s2,&Sp_b1,&fwd_s1);
  GLA_V_peq_M_times_V(&ans_s2,&Dp_b0,&fwd_s2);
  GLA_V_peq_M_times_V(&ans_s2,&Dp_b1,&fwd_s3);

  GLA_V_peq_M_times_V(&ans_s3,&Sp_b2,&fwd_s0);
  GLA_V_peq_M_times_V(&ans_s3,&Sp_b3,&fwd_s1);
  GLA_V_peq_M_times_V(&ans_s3,&Dp_b2,&fwd_s2);
  GLA_V_peq_M_times_V(&ans_s3,&Dp_b3,&fwd_s3);

  // -i ( i = 0,1,2 )
  GLA_V_peq_Ma_times_V(&pull_s0,&Dp_b0,&src_s0);
  GLA_V_peq_Ma_times_V(&pull_s0,&Dp_b2,&src_s1);
  GLA_V_meq_Ma_times_V(&pull_s0,&Sp_b0,&src_s2);
  GLA_V_meq_Ma_times_V(&pull_s0,&Sp_b2,&src_s3);
  
  GLA_V_peq_Ma_times_V(&pull_s1,&Dp_b1,&src_s0);
  GLA_V_peq_Ma_times_V(&pull_s1,&Dp_b3,&src_s1);
  GLA_V_meq_Ma_times_V(&pull_s1,&Sp_b1,&src_s2);
  GLA_V_meq_Ma_times_V(&pull_s1,&Sp_b3,&src_s3);
  
  GLA_V_meq_Ma_times_V(&pull_s2,&Sp_b0,&src_s0);
  GLA_V_meq_Ma_times_V(&pull_s2,&Sp_b2,&src_s1);
  GLA_V_peq_Ma_times_V(&pull_s2,&Dp_b0,&src_s2);
  GLA_V_peq_Ma_times_V(&pull_s2,&Dp_b2,&src_s3);
  
  GLA_V_meq_Ma_times_V(&pull_s3,&Sp_b1,&src_s0);
  GLA_V_meq_Ma_times_V(&pull_s3,&Sp_b3,&src_s1);
  GLA_V_peq_Ma_times_V(&pull_s3,&Dp_b1,&src_s2);
  GLA_V_peq_Ma_times_V(&pull_s3,&Dp_b3,&src_s3);
//  __syncthreads();

  //======================================================= 
  // Write Result QLA_ColorVector Through the Shared Memory
  //======================================================= 
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s0;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s0[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s1;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s1[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s2;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s2[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = ans_s3;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_ans_s3[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s0;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s0[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s1;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s1[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s2;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s2[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s3;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s3[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
//  __syncthreads();
}

//=========================================================================
// Kernel for 2-hop diagonal term 
//
//=========================================================================
__global__ void TwoHopDiag( int dir,
  GLA_Real *d_push_s0, GLA_Real *d_push_s1, 
  GLA_Real *d_push_s2, GLA_Real *d_push_s3, 
  GLA_Real *d_pull_s0, GLA_Real *d_pull_s1, 
  GLA_Real *d_pull_s2, GLA_Real *d_pull_s3, 
  GLA_Real *d_Dpp, GLA_Real *d_Spp, 
  GLA_Real *d_fwd_s0, GLA_Real *d_fwd_s1, 
  GLA_Real *d_fwd_s2, GLA_Real *d_fwd_s3, 
  GLA_Real *d_bck_s0, GLA_Real *d_bck_s1, 
  GLA_Real *d_bck_s2, GLA_Real *d_bck_s3 )
{

  //=================
  // Local Variables
  //=================
  //int tpb = blockDim.x;   // number of threads per block
  int tx = threadIdx.x;
  int sitesPerBlock = THREAD*blockIdx.x;
  int siteV = 6*sitesPerBlock + tx;
  int siteM = 18*sitesPerBlock + tx;
  int idx;

  GLA_ColorVector push_s0, push_s1, push_s2, push_s3;
  GLA_ColorVector pull_s0, pull_s1, pull_s2, pull_s3;
  GLA_ColorVector fwd_s0, fwd_s1, fwd_s2, fwd_s3;
  GLA_ColorVector bck_s0, bck_s1, bck_s2, bck_s3;
  GLA_ColorMatrix Dpp;
  GLA_ColorMatrix Spp;
  
  GLA_ColorVector zeroV = { .c0Re = 0., .c0Im = 0.,
                            .c1Re = 0., .c1Im = 0.,
                            .c2Re = 0., .c2Im = 0. };

  __shared__ GLA_Real s_tmp[THREAD*18];

  //================================================ 
  // Read QLA_ColorMatrix Through the Shared Memory
  //================================================ 
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Dpp[siteM+THREAD*idx];
  }
  __syncthreads();

    Dpp = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Spp[siteM+THREAD*idx];
  }
  __syncthreads();

    Spp = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  
  //======================================================= 
  // Read Source QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_bck_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    bck_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_bck_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    bck_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_bck_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    bck_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_bck_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    bck_s3 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s3 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();

  //======================================================= 
  // Read Result QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_pull_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    pull_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_pull_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    pull_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_pull_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    pull_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_pull_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    pull_s3 = ((GLA_ColorVector *)s_tmp)[tx];
//  __syncthreads();

  //============ 
  // Initialize
  //============ 
  push_s0 = zeroV;
  push_s1 = zeroV;
  push_s2 = zeroV;
  push_s3 = zeroV;
  
  //========
  // Dslash
  //========

  //----------------------
  // Diagonal Color Block
  //----------------------
  // +2i
  GLA_V_peq_M_times_V(&push_s0,&Dpp,&fwd_s0);
  GLA_V_peq_M_times_V(&push_s1,&Dpp,&fwd_s1);
  GLA_V_peq_M_times_V(&push_s2,&Dpp,&fwd_s2);
  GLA_V_peq_M_times_V(&push_s3,&Dpp,&fwd_s3);
  
  // -2i
  GLA_V_peq_Ma_times_V(&pull_s0,&Dpp,&bck_s0);
  GLA_V_peq_Ma_times_V(&pull_s1,&Dpp,&bck_s1);
  GLA_V_peq_Ma_times_V(&pull_s2,&Dpp,&bck_s2);
  GLA_V_peq_Ma_times_V(&pull_s3,&Dpp,&bck_s3);

  //-------------------------
  // Offdiagonal Color Block
  //-------------------------
  switch( dir ){
    case 0:
      // +0+0
      GLA_V_peq_M_times_V(&push_s0,&Spp,&fwd_s3);
      GLA_V_peq_M_times_V(&push_s1,&Spp,&fwd_s2);
      GLA_V_peq_M_times_V(&push_s2,&Spp,&fwd_s1);
      GLA_V_peq_M_times_V(&push_s3,&Spp,&fwd_s0);
      // -0-0
      GLA_V_meq_Ma_times_V(&pull_s0,&Spp,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s1,&Spp,&bck_s2);
      GLA_V_meq_Ma_times_V(&pull_s2,&Spp,&bck_s1);
      GLA_V_meq_Ma_times_V(&pull_s3,&Spp,&bck_s0);
      break;

    case 1:
      // +1+1
      GLA_V_peq_M_times_V(&push_s0,&Spp,&fwd_s3);
      GLA_V_meq_M_times_V(&push_s1,&Spp,&fwd_s2);
      GLA_V_peq_M_times_V(&push_s2,&Spp,&fwd_s1);
      GLA_V_meq_M_times_V(&push_s3,&Spp,&fwd_s0);
      // -1-1
      GLA_V_peq_Ma_times_V(&pull_s0,&Spp,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s1,&Spp,&bck_s2);
      GLA_V_peq_Ma_times_V(&pull_s2,&Spp,&bck_s1);
      GLA_V_meq_Ma_times_V(&pull_s3,&Spp,&bck_s0);
      break;

    case 2:
      // +2+2
      GLA_V_peq_M_times_V(&push_s0,&Spp,&fwd_s2);
      GLA_V_meq_M_times_V(&push_s1,&Spp,&fwd_s3);
      GLA_V_peq_M_times_V(&push_s2,&Spp,&fwd_s0);
      GLA_V_meq_M_times_V(&push_s3,&Spp,&fwd_s1);
      // -2-2
      GLA_V_meq_Ma_times_V(&pull_s0,&Spp,&bck_s2);
      GLA_V_peq_Ma_times_V(&pull_s1,&Spp,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s2,&Spp,&bck_s0);
      GLA_V_peq_Ma_times_V(&pull_s3,&Spp,&bck_s1);
      break;
  }
//  __syncthreads();

  //======================================================= 
  // Write Result QLA_ColorVector Through the Shared Memory
  //======================================================= 
    ((GLA_ColorVector *)s_tmp)[tx] = push_s0;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_push_s0[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = push_s1;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_push_s1[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = push_s2;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_push_s2[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = push_s3;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_push_s3[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s0;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s0[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s1;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s1[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s2;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s2[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s3;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s3[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
//  __syncthreads();
}

//=========================================================================
// Kernel for 2-hop offdiagonal term 
//
//=========================================================================
__global__ void TwoHopOffDiag( int dir,
  GLA_Real *d_push_s0, GLA_Real *d_push_s1, 
  GLA_Real *d_push_s2, GLA_Real *d_push_s3, 
  GLA_Real *d_pull_s0, GLA_Real *d_pull_s1, 
  GLA_Real *d_pull_s2, GLA_Real *d_pull_s3, 
  GLA_Real *d_Spm_b0, GLA_Real *d_Spm_b1, 
  GLA_Real *d_fwd_s0, GLA_Real *d_fwd_s1, 
  GLA_Real *d_fwd_s2, GLA_Real *d_fwd_s3, 
  GLA_Real *d_bck_s0, GLA_Real *d_bck_s1, 
  GLA_Real *d_bck_s2, GLA_Real *d_bck_s3 )
{

  //=================
  // Local Variables
  //=================
  //int tpb = blockDim.x;   // number of threads per block
  int tx = threadIdx.x;
  int sitesPerBlock = THREAD*blockIdx.x;
  int siteV = 6*sitesPerBlock + tx;
  int siteM = 18*sitesPerBlock + tx;
  int idx;

  GLA_ColorVector push_s0, push_s1, push_s2, push_s3;
  GLA_ColorVector pull_s0, pull_s1, pull_s2, pull_s3;
  GLA_ColorVector fwd_s0, fwd_s1, fwd_s2, fwd_s3;
  GLA_ColorVector bck_s0, bck_s1, bck_s2, bck_s3;
  GLA_ColorMatrix Spm_b0;
  GLA_ColorMatrix Spm_b1;

  __shared__ GLA_Real s_tmp[THREAD*18];

  //================================================ 
  // Read QLA_ColorMatrix Through the Shared Memory
  //================================================ 
  #pragma unroll
  for( idx = 0; idx < 18; idx++ ){
    s_tmp[tx+THREAD*idx] = d_Spm_b0[siteM+THREAD*idx];
  }
  __syncthreads();

    Spm_b0 = ((GLA_ColorMatrix *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  //-------------------------------------------------------
  if( d_Spm_b1 != NULL ){
    #pragma unroll
    for( idx = 0; idx < 18; idx++ ){
      s_tmp[tx+THREAD*idx] = d_Spm_b1[siteM+THREAD*idx];
    }
    __syncthreads();

      Spm_b1 = ((GLA_ColorMatrix *)s_tmp)[tx];
    __syncthreads();
  }

  //======================================================= 
  // Read Source QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_bck_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    bck_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_bck_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    bck_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_bck_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    bck_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_bck_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    bck_s3 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_fwd_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    fwd_s3 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();

  //======================================================= 
  // Read Result QLA_ColorVector Through the Shared Memory
  //======================================================= 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_pull_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    pull_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_pull_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    pull_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_pull_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    pull_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_pull_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    pull_s3 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_push_s0[siteV+THREAD*idx];
  }
  __syncthreads();

    push_s0 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_push_s1[siteV+THREAD*idx];
  }
  __syncthreads();

    push_s1 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_push_s2[siteV+THREAD*idx];
  }
  __syncthreads();

    push_s2 = ((GLA_ColorVector *)s_tmp)[tx];
  __syncthreads();
  //-------------------------------------------------------
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    s_tmp[tx+THREAD*idx] = d_push_s3[siteV+THREAD*idx];
  }
  __syncthreads();

    push_s3 = ((GLA_ColorVector *)s_tmp)[tx];
//  __syncthreads();

  //========
  // Dslash
  //========
  switch( dir ){
    case 1:
      // +0+1
      GLA_V_peq_M_times_V(&push_s0,&Spm_b0,&fwd_s3);
      GLA_V_peq_M_times_V(&push_s1,&Spm_b1,&fwd_s2);
      GLA_V_peq_M_times_V(&push_s2,&Spm_b0,&fwd_s1);
      GLA_V_peq_M_times_V(&push_s3,&Spm_b1,&fwd_s0);
      // -0-1 
      GLA_V_meq_Ma_times_V(&pull_s0,&Spm_b1,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s1,&Spm_b0,&bck_s2);
      GLA_V_meq_Ma_times_V(&pull_s2,&Spm_b1,&bck_s1);
      GLA_V_meq_Ma_times_V(&pull_s3,&Spm_b0,&bck_s0);
      break;

    case 2:
      // +0+2
      GLA_V_peq_M_times_V(&push_s0,&Spm_b0,&fwd_s2);
      GLA_V_peq_M_times_V(&push_s0,&Spm_b0,&fwd_s3);
      GLA_V_peq_M_times_V(&push_s1,&Spm_b0,&fwd_s2);
      GLA_V_meq_M_times_V(&push_s1,&Spm_b0,&fwd_s3);
      GLA_V_peq_M_times_V(&push_s2,&Spm_b0,&fwd_s0);
      GLA_V_peq_M_times_V(&push_s2,&Spm_b0,&fwd_s1);
      GLA_V_peq_M_times_V(&push_s3,&Spm_b0,&fwd_s0);
      GLA_V_meq_M_times_V(&push_s3,&Spm_b0,&fwd_s1);
      // -0-2
      GLA_V_meq_Ma_times_V(&pull_s0,&Spm_b0,&bck_s2);
      GLA_V_meq_Ma_times_V(&pull_s0,&Spm_b0,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s1,&Spm_b0,&bck_s2);
      GLA_V_peq_Ma_times_V(&pull_s1,&Spm_b0,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s2,&Spm_b0,&bck_s0);
      GLA_V_meq_Ma_times_V(&pull_s2,&Spm_b0,&bck_s1);
      GLA_V_meq_Ma_times_V(&pull_s3,&Spm_b0,&bck_s0);
      GLA_V_peq_Ma_times_V(&pull_s3,&Spm_b0,&bck_s1);
      break;

    case 3:
      // +1+2
      GLA_V_peq_M_times_V(&push_s0,&Spm_b0,&fwd_s2);
      GLA_V_peq_M_times_V(&push_s0,&Spm_b1,&fwd_s3);
      GLA_V_meq_M_times_V(&push_s1,&Spm_b1,&fwd_s2);
      GLA_V_meq_M_times_V(&push_s1,&Spm_b0,&fwd_s3);
      GLA_V_peq_M_times_V(&push_s2,&Spm_b0,&fwd_s0);
      GLA_V_peq_M_times_V(&push_s2,&Spm_b1,&fwd_s1);
      GLA_V_meq_M_times_V(&push_s3,&Spm_b1,&fwd_s0);
      GLA_V_meq_M_times_V(&push_s3,&Spm_b0,&fwd_s1);
      // -1-2
      GLA_V_meq_Ma_times_V(&pull_s0,&Spm_b0,&bck_s2);
      GLA_V_peq_Ma_times_V(&pull_s0,&Spm_b1,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s1,&Spm_b1,&bck_s2);
      GLA_V_peq_Ma_times_V(&pull_s1,&Spm_b0,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s2,&Spm_b0,&bck_s0);
      GLA_V_peq_Ma_times_V(&pull_s2,&Spm_b1,&bck_s1);
      GLA_V_meq_Ma_times_V(&pull_s3,&Spm_b1,&bck_s0);
      GLA_V_peq_Ma_times_V(&pull_s3,&Spm_b0,&bck_s1);
      break;

    case 4:
      // +0-1
      GLA_V_peq_M_times_V(&push_s0,&Spm_b0,&fwd_s3);
      GLA_V_peq_M_times_V(&push_s1,&Spm_b1,&fwd_s2);
      GLA_V_peq_M_times_V(&push_s2,&Spm_b0,&fwd_s1);
      GLA_V_peq_M_times_V(&push_s3,&Spm_b1,&fwd_s0);
      // -0+1
      GLA_V_meq_Ma_times_V(&pull_s0,&Spm_b1,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s1,&Spm_b0,&bck_s2);
      GLA_V_meq_Ma_times_V(&pull_s2,&Spm_b1,&bck_s1);
      GLA_V_meq_Ma_times_V(&pull_s3,&Spm_b0,&bck_s0);
      break;

    case 5:
      // +0-2
      GLA_V_meq_M_times_V(&push_s0,&Spm_b0,&fwd_s2);
      GLA_V_peq_M_times_V(&push_s0,&Spm_b0,&fwd_s3);
      GLA_V_peq_M_times_V(&push_s1,&Spm_b0,&fwd_s2);
      GLA_V_peq_M_times_V(&push_s1,&Spm_b0,&fwd_s3);
      GLA_V_meq_M_times_V(&push_s2,&Spm_b0,&fwd_s0);
      GLA_V_peq_M_times_V(&push_s2,&Spm_b0,&fwd_s1);
      GLA_V_peq_M_times_V(&push_s3,&Spm_b0,&fwd_s0);
      GLA_V_peq_M_times_V(&push_s3,&Spm_b0,&fwd_s1);
      // -0+2
      GLA_V_peq_Ma_times_V(&pull_s0,&Spm_b0,&bck_s2);
      GLA_V_meq_Ma_times_V(&pull_s0,&Spm_b0,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s1,&Spm_b0,&bck_s2);
      GLA_V_meq_Ma_times_V(&pull_s1,&Spm_b0,&bck_s3);
      GLA_V_peq_Ma_times_V(&pull_s2,&Spm_b0,&bck_s0);
      GLA_V_meq_Ma_times_V(&pull_s2,&Spm_b0,&bck_s1);
      GLA_V_meq_Ma_times_V(&pull_s3,&Spm_b0,&bck_s0);
      GLA_V_meq_Ma_times_V(&pull_s3,&Spm_b0,&bck_s1);
      break;

    case 6:
      // +1-2
      GLA_V_meq_M_times_V(&push_s0,&Spm_b0,&fwd_s2);
      GLA_V_peq_M_times_V(&push_s0,&Spm_b1,&fwd_s3);
      GLA_V_meq_M_times_V(&push_s1,&Spm_b1,&fwd_s2);
      GLA_V_peq_M_times_V(&push_s1,&Spm_b0,&fwd_s3);
      GLA_V_meq_M_times_V(&push_s2,&Spm_b0,&fwd_s0);
      GLA_V_peq_M_times_V(&push_s2,&Spm_b1,&fwd_s1);
      GLA_V_meq_M_times_V(&push_s3,&Spm_b1,&fwd_s0);
      GLA_V_peq_M_times_V(&push_s3,&Spm_b0,&fwd_s1);
      // -1+2
      GLA_V_peq_Ma_times_V(&pull_s0,&Spm_b0,&bck_s2);
      GLA_V_peq_Ma_times_V(&pull_s0,&Spm_b1,&bck_s3);
      GLA_V_meq_Ma_times_V(&pull_s1,&Spm_b1,&bck_s2);
      GLA_V_meq_Ma_times_V(&pull_s1,&Spm_b0,&bck_s3);
      GLA_V_peq_Ma_times_V(&pull_s2,&Spm_b0,&bck_s0);
      GLA_V_peq_Ma_times_V(&pull_s2,&Spm_b1,&bck_s1);
      GLA_V_meq_Ma_times_V(&pull_s3,&Spm_b1,&bck_s0);
      GLA_V_meq_Ma_times_V(&pull_s3,&Spm_b0,&bck_s1);
      break;
  }
//  __syncthreads();

  //======================================================= 
  // Write Result QLA_ColorVector Through the Shared Memory
  //======================================================= 
    ((GLA_ColorVector *)s_tmp)[tx] = push_s0;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_push_s0[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = push_s1;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_push_s1[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = push_s2;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_push_s2[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = push_s3;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_push_s3[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s0;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s0[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s1;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s1[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s2;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s2[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
  __syncthreads();
  //-------------------------------------------------------
    ((GLA_ColorVector *)s_tmp)[tx] = pull_s3;
  __syncthreads();
 
  #pragma unroll
  for( idx = 0; idx < 6; idx++ ){
    d_pull_s3[siteV+THREAD*idx] = s_tmp[tx+THREAD*idx];
  }
//  __syncthreads();
}

//=========================================================================
// Interface between host(CPU) and device(GPU)
//
//=========================================================================
extern "C" void okaction_dslash_kernel_cuda(
  GLA_Real *h_kap,
  GLA_Real *h_D0[], GLA_Real *h_S0[], 
  GLA_Real *h_U3, 
  GLA_Real *h_Dp0[], GLA_Real *h_Dp1[], GLA_Real *h_Dp2[],
  GLA_Real *h_Sp0[], GLA_Real *h_Sp1[], GLA_Real *h_Sp2[], GLA_Real *h_Sp3[],
  GLA_Real *h_Dpp[], GLA_Real *h_Spp[], GLA_Real *h_Spm[],
  GLA_Real *h_src[], 
  GLA_Real *h_fwd0[], GLA_Real *h_fwd1[], 
  GLA_Real *h_fwd2[], GLA_Real *h_fwd3[],
  GLA_Real *h_bck0[], GLA_Real *h_bck1[], GLA_Real *h_bck2[], 
  GLA_Real *h_ans[], 
  GLA_Real *h_push0[], GLA_Real *h_push1[], GLA_Real *h_push2[], 
  GLA_Real *h_pull0[], GLA_Real *h_pull1[], 
  GLA_Real *h_pull2[], GLA_Real *h_pull3[], 
  int sites_on_node, int arrange, 
  struct xxx_profile *load_link, struct xxx_profile *load_fermion,
  struct xxx_profile *cuda_kernel)
{
  
  //=================
  // Local Variables
  //=================
  int s;
  bool load_precalculation = true;
  int offsetV = 6*cuda_volume;
  int offsetM = 18*cuda_volume;
  size_t sizeV = sizeof(GLA_Real)*offsetV;
  size_t sizeM = sizeof(GLA_Real)*offsetM;

  GLA_Real *d_D0[2], *d_S0[2];
  GLA_Real *d_U3; 
  GLA_Real *d_Dp0[4], *d_Dp1[4], *d_Dp2[4];
  GLA_Real *d_Sp0[4], *d_Sp1[4], *d_Sp2[4], *d_Sp3[4];
  GLA_Real *d_Dpp[3], *d_Spp[3], *d_Spm[10];
  
  GLA_Real *d_src[4], *d_ans[4];
  GLA_Real *d_fwd0[4], *d_fwd1[4], *d_fwd2[4], *d_fwd3[4];
  GLA_Real *d_bck0[4], *d_bck1[4], *d_bck2[4];
  GLA_Real *d_push0[4], *d_push1[4], *d_push2[4];
  GLA_Real *d_pull0[4], *d_pull1[4], *d_pull2[4], *d_pull3[4];



  //==============
  // Device Setup
  //==============
  dim3 Nthread(THREAD,1,1);
  dim3 Nblock(cuda_volume/THREAD,1,1);
  
  //===============================================================
  // Device Memory Assignment
  // : Memory allocation was done outside this dslash kernel.
  //   So only the type casting is necessary for single precision.
  //===============================================================
#if GLA_PrecisionInt == 1
  if( cuda_precision != CUDA_SINGLE ){
    cuda_precision = CUDA_SINGLE;
  }else{
    load_precalculation = false;
    // save the precalculation on the device memory
    // and reuse it over the CG iterations
  }
  
//  d_D0[0] = (GLA_Real *)d_Mat[0];
//  d_D0[1] = (GLA_Real *)d_Mat[1];
//  
//  d_S0[0] = (GLA_Real *)d_Mat[2];
//  d_S0[1] = (GLA_Real *)d_Mat[3];
//  
//  d_Sp3[0] = (GLA_Real *)d_Mat[4];
//  d_Sp3[1] = (GLA_Real *)d_Mat[5];
//  d_Sp3[2] = (GLA_Real *)d_Mat[6];
//  d_Sp3[3] = (GLA_Real *)d_Mat[7];
//  
//  d_U3 = (GLA_Real *)d_Mat[8];
//  
//  d_src[0] = (GLA_Real *)d_Vec[0];
//  d_src[1] = (GLA_Real *)d_Vec[1];
//  d_src[2] = (GLA_Real *)d_Vec[2];
//  d_src[3] = (GLA_Real *)d_Vec[3];
// 
//  d_ans[0] = (GLA_Real *)d_Vec[4];
//  d_ans[1] = (GLA_Real *)d_Vec[5];
//  d_ans[2] = (GLA_Real *)d_Vec[6];
//  d_ans[3] = (GLA_Real *)d_Vec[7];
//  
//  d_fwd3[0] = (GLA_Real *)d_Vec[8];
//  d_fwd3[1] = (GLA_Real *)d_Vec[9];
//  d_fwd3[2] = (GLA_Real *)d_Vec[10];
//  d_fwd3[3] = (GLA_Real *)d_Vec[11];
// 
//  d_pull3[0] = (GLA_Real *)d_Vec[12];
//  d_pull3[1] = (GLA_Real *)d_Vec[13];
//  d_pull3[2] = (GLA_Real *)d_Vec[14];
//  d_pull3[3] = (GLA_Real *)d_Vec[15];
  
  d_D0[0] = (GLA_Real *)d_Mat[0];
  d_D0[1] = ((GLA_Real *)d_Mat[0])+offsetM;
  
  d_S0[0] = (GLA_Real *)d_Mat[1];
  d_S0[1] = ((GLA_Real *)d_Mat[1])+offsetM;
  
  d_Sp3[0] = (GLA_Real *)d_Mat[2];
  d_Sp3[1] = (GLA_Real *)d_Mat[3];
  d_Sp3[2] = ((GLA_Real *)d_Mat[2])+offsetM;
  d_Sp3[3] = ((GLA_Real *)d_Mat[3])+offsetM;
  
  d_U3 = (GLA_Real *)d_Mat[24];
  
  d_Dp0[0] = (GLA_Real *)d_Mat[4];
  d_Dp0[1] = (GLA_Real *)d_Mat[5];
  d_Dp0[2] = ((GLA_Real *)d_Mat[4])+offsetM;
  d_Dp0[3] = ((GLA_Real *)d_Mat[5])+offsetM;
  
  d_Sp0[0] = (GLA_Real *)d_Mat[6];
  d_Sp0[1] = (GLA_Real *)d_Mat[7];
  d_Sp0[2] = ((GLA_Real *)d_Mat[6])+offsetM;
  d_Sp0[3] = ((GLA_Real *)d_Mat[7])+offsetM;
  
  d_Dp1[0] = (GLA_Real *)d_Mat[8];
  d_Dp1[1] = (GLA_Real *)d_Mat[9];
  d_Dp1[2] = ((GLA_Real *)d_Mat[8])+offsetM;
  d_Dp1[3] = ((GLA_Real *)d_Mat[9])+offsetM;
  
  d_Sp1[0] = (GLA_Real *)d_Mat[10];
  d_Sp1[1] = (GLA_Real *)d_Mat[11];
  d_Sp1[2] = ((GLA_Real *)d_Mat[10])+offsetM;
  d_Sp1[3] = ((GLA_Real *)d_Mat[11])+offsetM;
  
  d_Dp2[0] = (GLA_Real *)d_Mat[12];
  d_Dp2[1] = (GLA_Real *)d_Mat[13];
  d_Dp2[2] = ((GLA_Real *)d_Mat[12])+offsetM;
  d_Dp2[3] = ((GLA_Real *)d_Mat[13])+offsetM;
  
  d_Sp2[0] = (GLA_Real *)d_Mat[14];
  d_Sp2[1] = (GLA_Real *)d_Mat[15];
  d_Sp2[2] = ((GLA_Real *)d_Mat[14])+offsetM;
  d_Sp2[3] = ((GLA_Real *)d_Mat[15])+offsetM;

  d_Dpp[0] = (GLA_Real *)d_Mat[16];
  d_Dpp[1] = (GLA_Real *)d_Mat[17];
  d_Dpp[2] = (GLA_Real *)d_Mat[18];
  
  d_Spp[0] = ((GLA_Real *)d_Mat[16])+offsetM;
  d_Spp[1] = ((GLA_Real *)d_Mat[17])+offsetM;
  d_Spp[2] = ((GLA_Real *)d_Mat[18])+offsetM;
  
  d_Spm[0] = (GLA_Real *)d_Mat[19];
  d_Spm[1] = (GLA_Real *)d_Mat[20];
  d_Spm[2] = (GLA_Real *)d_Mat[21];
  d_Spm[3] = (GLA_Real *)d_Mat[22];
  d_Spm[4] = (GLA_Real *)d_Mat[23];
  
  d_Spm[5] = ((GLA_Real *)d_Mat[19])+offsetM;
  d_Spm[6] = ((GLA_Real *)d_Mat[20])+offsetM;
  d_Spm[7] = ((GLA_Real *)d_Mat[21])+offsetM;
  d_Spm[8] = ((GLA_Real *)d_Mat[22])+offsetM;
  d_Spm[9] = ((GLA_Real *)d_Mat[23])+offsetM;
  
  d_src[0] = (GLA_Real *)d_Vec[0];
  d_src[1] = (GLA_Real *)d_Vec[1];
  d_src[2] = (GLA_Real *)d_Vec[2];
  d_src[3] = (GLA_Real *)d_Vec[3];
 
  d_ans[0] = ((GLA_Real *)d_Vec[0])+offsetV;
  d_ans[1] = ((GLA_Real *)d_Vec[1])+offsetV;
  d_ans[2] = ((GLA_Real *)d_Vec[2])+offsetV;
  d_ans[3] = ((GLA_Real *)d_Vec[3])+offsetV;
  
  d_fwd3[0] = (GLA_Real *)d_Vec[4];
  d_fwd3[1] = (GLA_Real *)d_Vec[5];
  d_fwd3[2] = (GLA_Real *)d_Vec[6];
  d_fwd3[3] = (GLA_Real *)d_Vec[7];
 
  d_pull3[0] = ((GLA_Real *)d_Vec[4])+offsetV;
  d_pull3[1] = ((GLA_Real *)d_Vec[5])+offsetV;
  d_pull3[2] = ((GLA_Real *)d_Vec[6])+offsetV;
  d_pull3[3] = ((GLA_Real *)d_Vec[7])+offsetV;

  d_fwd0[0] = (GLA_Real *)d_Vec[8];
  d_fwd0[1] = (GLA_Real *)d_Vec[9];
  d_fwd0[2] = (GLA_Real *)d_Vec[10];
  d_fwd0[3] = (GLA_Real *)d_Vec[11];
 
  d_pull0[0] = ((GLA_Real *)d_Vec[8])+offsetV;
  d_pull0[1] = ((GLA_Real *)d_Vec[9])+offsetV;
  d_pull0[2] = ((GLA_Real *)d_Vec[10])+offsetV;
  d_pull0[3] = ((GLA_Real *)d_Vec[11])+offsetV;

  d_fwd1[0] = (GLA_Real *)d_Vec[12];
  d_fwd1[1] = (GLA_Real *)d_Vec[13];
  d_fwd1[2] = (GLA_Real *)d_Vec[14];
  d_fwd1[3] = (GLA_Real *)d_Vec[15];
 
  d_pull1[0] = ((GLA_Real *)d_Vec[12])+offsetV;
  d_pull1[1] = ((GLA_Real *)d_Vec[13])+offsetV;
  d_pull1[2] = ((GLA_Real *)d_Vec[14])+offsetV;
  d_pull1[3] = ((GLA_Real *)d_Vec[15])+offsetV;

  d_fwd2[0] = (GLA_Real *)d_Vec[16];
  d_fwd2[1] = (GLA_Real *)d_Vec[17];
  d_fwd2[2] = (GLA_Real *)d_Vec[18];
  d_fwd2[3] = (GLA_Real *)d_Vec[19];
 
  d_pull2[0] = ((GLA_Real *)d_Vec[16])+offsetV;
  d_pull2[1] = ((GLA_Real *)d_Vec[17])+offsetV;
  d_pull2[2] = ((GLA_Real *)d_Vec[18])+offsetV;
  d_pull2[3] = ((GLA_Real *)d_Vec[19])+offsetV;

  d_bck0[0] = (GLA_Real *)d_Vec[20];
  d_bck0[1] = (GLA_Real *)d_Vec[21];
  d_bck0[2] = (GLA_Real *)d_Vec[22];
  d_bck0[3] = (GLA_Real *)d_Vec[23];

  d_push0[0] = ((GLA_Real *)d_Vec[20])+offsetV;
  d_push0[1] = ((GLA_Real *)d_Vec[21])+offsetV;
  d_push0[2] = ((GLA_Real *)d_Vec[22])+offsetV;
  d_push0[3] = ((GLA_Real *)d_Vec[23])+offsetV;

  d_bck1[0] = (GLA_Real *)d_Vec[24];
  d_bck1[1] = (GLA_Real *)d_Vec[25];
  d_bck1[2] = (GLA_Real *)d_Vec[26];
  d_bck1[3] = (GLA_Real *)d_Vec[27];

  d_push1[0] = ((GLA_Real *)d_Vec[24])+offsetV;
  d_push1[1] = ((GLA_Real *)d_Vec[25])+offsetV;
  d_push1[2] = ((GLA_Real *)d_Vec[26])+offsetV;
  d_push1[3] = ((GLA_Real *)d_Vec[27])+offsetV;

  d_bck2[0] = (GLA_Real *)d_Vec[28];
  d_bck2[1] = (GLA_Real *)d_Vec[29];
  d_bck2[2] = (GLA_Real *)d_Vec[30];
  d_bck2[3] = (GLA_Real *)d_Vec[31];

  d_push2[0] = ((GLA_Real *)d_Vec[28])+offsetV;
  d_push2[1] = ((GLA_Real *)d_Vec[29])+offsetV;
  d_push2[2] = ((GLA_Real *)d_Vec[30])+offsetV;
  d_push2[3] = ((GLA_Real *)d_Vec[31])+offsetV;
#elif GLA_PrecisionInt == 2
#ifdef XXX_CUDA_LARGE_DEVICE_MEM
  if( cuda_precision != CUDA_DOUBLE ){
    cuda_precision = CUDA_DOUBLE;
  }else{
    load_precalculation = false;
    // save the precalculation on the device memory
    // and reuse it over the CG iterations
  }
#else 
  if( cuda_precision != CUDA_DOUBLE ){
    cuda_precision = CUDA_DOUBLE;
    // always reload the precalculation
  }
#endif
  d_D0[0] = d_Mat[0];
  d_D0[1] = d_Mat[1];
  
  d_S0[0] = d_Mat[2];
  d_S0[1] = d_Mat[3];
  
  d_Sp3[0] = d_Mat[4];
  d_Sp3[1] = d_Mat[5];
  d_Sp3[2] = d_Mat[6];
  d_Sp3[3] = d_Mat[7];
  
  d_U3 = d_Mat[8];
  
  d_Dp0[0] = d_Mat[9];
  d_Dp0[1] = d_Mat[10];
  d_Dp0[2] = d_Mat[11];
  d_Dp0[3] = d_Mat[12];
  
  d_Sp0[0] = d_Mat[13];
  d_Sp0[1] = d_Mat[14];
  d_Sp0[2] = d_Mat[15];
  d_Sp0[3] = d_Mat[16];
  
  d_Dp1[0] = d_Mat[17];
  d_Dp1[1] = d_Mat[18];
  d_Dp1[2] = d_Mat[19];
  d_Dp1[3] = d_Mat[20];
  
  d_Sp1[0] = d_Mat[21];
  d_Sp1[1] = d_Mat[22];
  d_Sp1[2] = d_Mat[23];
  d_Sp1[3] = d_Mat[24];
  
  d_Dp2[0] = d_Mat[0];
  d_Dp2[1] = d_Mat[1];
  d_Dp2[2] = d_Mat[2];
  d_Dp2[3] = d_Mat[3];
  
  d_Sp2[0] = d_Mat[4];
  d_Sp2[1] = d_Mat[5];
  d_Sp2[2] = d_Mat[6];
  d_Sp2[3] = d_Mat[7];

  d_Dpp[0] = d_Mat[8];
  d_Dpp[1] = d_Mat[9];
  d_Dpp[2] = d_Mat[10];
  
  d_Spp[0] = d_Mat[11];
  d_Spp[1] = d_Mat[12];
  d_Spp[2] = d_Mat[13];
  
  d_Spm[0] = d_Mat[14];
  d_Spm[1] = d_Mat[15];
  d_Spm[2] = d_Mat[16];
  d_Spm[3] = d_Mat[17];
  d_Spm[4] = d_Mat[18];
  
  d_Spm[5] = d_Mat[19];
  d_Spm[6] = d_Mat[20];
  d_Spm[7] = d_Mat[21];
  d_Spm[8] = d_Mat[22];
  d_Spm[9] = d_Mat[23];
  
  d_fwd3[0] = d_Vec[0];
  d_fwd3[1] = d_Vec[1];
  d_fwd3[2] = d_Vec[2];
  d_fwd3[3] = d_Vec[3];
  
  d_pull3[0] = d_Vec[4];
  d_pull3[1] = d_Vec[5];
  d_pull3[2] = d_Vec[6];
  d_pull3[3] = d_Vec[7];
  
  d_src[0] = d_Vec[8];
  d_src[1] = d_Vec[9];
  d_src[2] = d_Vec[10];
  d_src[3] = d_Vec[11];
  
  d_ans[0] = d_Vec[12];
  d_ans[1] = d_Vec[13];
  d_ans[2] = d_Vec[14];
  d_ans[3] = d_Vec[15];
  
  d_fwd0[0] = d_Vec[16];
  d_fwd0[1] = d_Vec[17];
  d_fwd0[2] = d_Vec[18];
  d_fwd0[3] = d_Vec[19];
  
  d_pull0[0] = d_Vec[20];
  d_pull0[1] = d_Vec[21];
  d_pull0[2] = d_Vec[22];
  d_pull0[3] = d_Vec[23];
  
  d_fwd1[0] = d_Vec[24];
  d_fwd1[1] = d_Vec[25];
  d_fwd1[2] = d_Vec[26];
  d_fwd1[3] = d_Vec[27];
  
  d_pull1[0] = d_Vec[28];
  d_pull1[1] = d_Vec[29];
  d_pull1[2] = d_Vec[30];
  d_pull1[3] = d_Vec[31];
  
  d_fwd2[0] = d_Vec[0];
  d_fwd2[1] = d_Vec[1];
  d_fwd2[2] = d_Vec[2];
  d_fwd2[3] = d_Vec[3];
  
  d_pull2[0] = d_Vec[4];
  d_pull2[1] = d_Vec[5];
  d_pull2[2] = d_Vec[6];
  d_pull2[3] = d_Vec[7];

  d_bck0[0] = d_Vec[8];
  d_bck0[1] = d_Vec[9];
  d_bck0[2] = d_Vec[10];
  d_bck0[3] = d_Vec[11];

  d_push0[0] = d_Vec[12];
  d_push0[1] = d_Vec[13];
  d_push0[2] = d_Vec[14];
  d_push0[3] = d_Vec[15];

  d_bck1[0] = d_Vec[32];
  d_bck1[1] = d_Vec[33];
  d_bck1[2] = d_Vec[34];
  d_bck1[3] = d_Vec[35];

  d_push1[0] = d_Vec[36];
  d_push1[1] = d_Vec[37];
  d_push1[2] = d_Vec[38];
  d_push1[3] = d_Vec[39];

  d_bck2[0] = d_Vec[40];
  d_bck2[1] = d_Vec[41];
  d_bck2[2] = d_Vec[42];
  d_bck2[3] = d_Vec[43];

  d_push2[0] = d_Vec[44];
  d_push2[1] = d_Vec[45];
  d_push2[2] = d_Vec[46];
  d_push2[3] = d_Vec[47];
#endif

  //XXX_profile(cuda_kernel);
  //=======
  // 0-hop 
  //=======
  if( load_precalculation ){
    XXX_profile(load_link);
    #pragma unroll
    for( s = 0; s < 2; s++ ){
      CheckCUDA(cudaMemcpy(d_D0[s], h_D0[s], sizeM, cudaMemcpyHostToDevice));
      CheckCUDA(cudaMemcpy(d_S0[s], h_S0[s], sizeM, cudaMemcpyHostToDevice));
    }
    CheckCUDA(cudaMemcpyToSymbol(c_kap,h_kap,sizeof(GLA_Real)));
    XXX_profile(load_link);
  }

  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(d_src[s], h_src[s], sizeV, cudaMemcpyHostToDevice));
  }
  XXX_profile(load_fermion);

  XXX_profile(cuda_kernel);
  OnSite<<<Nblock,Nthread>>>(d_ans[0], d_ans[1], d_ans[2], d_ans[3],
                             d_D0[0], d_D0[1], 
                             d_S0[0], d_S0[1],
                             d_src[0], d_src[1], d_src[2], d_src[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);
  
  //=======
  // 1-hop 
  //=======
  if( load_precalculation ){ 
    XXX_profile(load_link);
    #pragma unroll
    for( s = 0; s < 4; s++ ){
      CheckCUDA(cudaMemcpy(d_Sp3[s], h_Sp3[s], sizeM, cudaMemcpyHostToDevice));
    }
    CheckCUDA(cudaMemcpy(d_U3, h_U3, sizeM, cudaMemcpyHostToDevice));
    XXX_profile(load_link);
  }

  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(d_fwd3[s], h_fwd3[s], sizeV, cudaMemcpyHostToDevice));
  }
  XXX_profile(load_fermion);

  XXX_profile(cuda_kernel);
  OneHopTime<<<Nblock,Nthread>>>(d_ans[0], d_ans[1], d_ans[2], d_ans[3], 
                                 d_pull3[0], d_pull3[1], 
                                 d_pull3[2], d_pull3[3], 
                                 d_Sp3[0], d_Sp3[1], d_Sp3[2], d_Sp3[3], 
                                 d_U3,
                                 d_fwd3[0], d_fwd3[1], 
                                 d_fwd3[2], d_fwd3[3], 
                                 d_src[0], d_src[1], d_src[2], d_src[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);

  if( load_precalculation ){ 
    XXX_profile(load_link);
    #pragma unroll
    for( s = 0; s < 4; s++ ){
      CheckCUDA(cudaMemcpy(d_Dp0[s], h_Dp0[s], sizeM, 
                           cudaMemcpyHostToDevice));
      CheckCUDA(cudaMemcpy(d_Sp0[s], h_Sp0[s], sizeM, 
                           cudaMemcpyHostToDevice));
    }
    XXX_profile(load_link);
  }

  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(d_fwd0[s], h_fwd0[s], sizeV, 
                         cudaMemcpyHostToDevice));
  }
  XXX_profile(load_fermion);
  
  XXX_profile(cuda_kernel);
  OneHopSpace<<<Nblock,Nthread>>>(d_ans[0], d_ans[1], d_ans[2], d_ans[3], 
                                  d_pull0[0], d_pull0[1], 
                                  d_pull0[2], d_pull0[3], 
                                  d_Dp0[0], d_Dp0[1], d_Dp0[2], d_Dp0[3],
                                  d_Sp0[0], d_Sp0[1], d_Sp0[2], d_Sp0[3],
                                  d_fwd0[0], d_fwd0[1], 
                                  d_fwd0[2], d_fwd0[3], 
                                  d_src[0], d_src[1], d_src[2], d_src[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);

  if( load_precalculation ){ 
    XXX_profile(load_link);
    #pragma unroll
    for( s = 0; s < 4; s++ ){
      CheckCUDA(cudaMemcpy(d_Dp1[s], h_Dp1[s], sizeM, 
                           cudaMemcpyHostToDevice));
      CheckCUDA(cudaMemcpy(d_Sp1[s], h_Sp1[s], sizeM, 
                           cudaMemcpyHostToDevice));
    }
    XXX_profile(load_link);
  }

  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(d_fwd1[s], h_fwd1[s], sizeV, 
                         cudaMemcpyHostToDevice));
  }
  XXX_profile(load_fermion);
  
  XXX_profile(cuda_kernel);
  OneHopSpace<<<Nblock,Nthread>>>(d_ans[0], d_ans[1], d_ans[2], d_ans[3], 
                                  d_pull1[0], d_pull1[1], 
                                  d_pull1[2], d_pull1[3], 
                                  d_Dp1[0], d_Dp1[1], d_Dp1[2], d_Dp1[3],
                                  d_Sp1[0], d_Sp1[1], d_Sp1[2], d_Sp1[3],
                                  d_fwd1[0], d_fwd1[1], 
                                  d_fwd1[2], d_fwd1[3], 
                                  d_src[0], d_src[1], d_src[2], d_src[3]);
  cudaThreadSynchronize(); //Do not comment out
  XXX_profile(cuda_kernel);
  
  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(h_pull3[s], d_pull3[s], sizeV, 
                         cudaMemcpyDeviceToHost));
  }
  XXX_profile(load_fermion);

  if( load_precalculation ){ 
    XXX_profile(load_link);
    #pragma unroll
    for( s = 0; s < 4; s++ ){
      CheckCUDA(cudaMemcpy(d_Dp2[s], h_Dp2[s], sizeM, 
                           cudaMemcpyHostToDevice));
      CheckCUDA(cudaMemcpy(d_Sp2[s], h_Sp2[s], sizeM, 
                           cudaMemcpyHostToDevice));
    }
    XXX_profile(load_link);
  }

  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(d_fwd2[s], h_fwd2[s], sizeV, 
                         cudaMemcpyHostToDevice));
  }
  XXX_profile(load_fermion);
  
  XXX_profile(cuda_kernel);
  OneHopSpace<<<Nblock,Nthread>>>(d_ans[0], d_ans[1], d_ans[2], d_ans[3], 
                                  d_pull2[0], d_pull2[1], 
                                  d_pull2[2], d_pull2[3], 
                                  d_Dp2[0], d_Dp2[1], d_Dp2[2], d_Dp2[3],
                                  d_Sp2[0], d_Sp2[1], d_Sp2[2], d_Sp2[3],
                                  d_fwd2[0], d_fwd2[1], 
                                  d_fwd2[2], d_fwd2[3], 
                                  d_src[0], d_src[1], d_src[2], d_src[3]);
  cudaThreadSynchronize(); //Do not comment out
  XXX_profile(cuda_kernel);

  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(h_ans[s], d_ans[s], sizeV, cudaMemcpyDeviceToHost));
  }
  XXX_profile(load_fermion);
  
  //=======
  // 2-hop 
  //=======
  if( load_precalculation ){ 
    XXX_profile(load_link);
    CheckCUDA(cudaMemcpy(d_Dpp[0], h_Dpp[0], sizeM, cudaMemcpyHostToDevice));
    CheckCUDA(cudaMemcpy(d_Spp[0], h_Spp[0], sizeM, cudaMemcpyHostToDevice));
    XXX_profile(load_link);
  }

  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(d_bck0[s], h_bck0[s], sizeV, 
                         cudaMemcpyHostToDevice));
  }
  XXX_profile(load_fermion);

  XXX_profile(cuda_kernel);
  TwoHopDiag<<<Nblock,Nthread>>>( 0,
                                 d_push0[0], d_push0[1], 
                                 d_push0[2], d_push0[3], 
                                 d_pull0[0], d_pull0[1], 
                                 d_pull0[2], d_pull0[3], 
                                 d_Dpp[0], d_Spp[0],  
                                 d_fwd0[0], d_fwd0[1], d_fwd0[2], d_fwd0[3], 
                                 d_bck0[0], d_bck0[1], d_bck0[2], d_bck0[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);
  
  if( load_precalculation ){ 
    XXX_profile(load_link);
    CheckCUDA(cudaMemcpy(d_Dpp[1], h_Dpp[1], sizeM, cudaMemcpyHostToDevice));
    CheckCUDA(cudaMemcpy(d_Spp[1], h_Spp[1], sizeM, cudaMemcpyHostToDevice));
    XXX_profile(load_link);
  }

  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(d_bck1[s], h_bck1[s], sizeV, 
                         cudaMemcpyHostToDevice));
  }
  XXX_profile(load_fermion);

  XXX_profile(cuda_kernel);
  TwoHopDiag<<<Nblock,Nthread>>>( 1,
                                 d_push1[0], d_push1[1], 
                                 d_push1[2], d_push1[3], 
                                 d_pull1[0], d_pull1[1], 
                                 d_pull1[2], d_pull1[3], 
                                 d_Dpp[1], d_Spp[1],  
                                 d_fwd1[0], d_fwd1[1], d_fwd1[2], d_fwd1[3], 
                                 d_bck1[0], d_bck1[1], d_bck1[2], d_bck1[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);
  
  if( load_precalculation ){ 
    XXX_profile(load_link);
    CheckCUDA(cudaMemcpy(d_Dpp[2], h_Dpp[2], sizeM, cudaMemcpyHostToDevice));
    CheckCUDA(cudaMemcpy(d_Spp[2], h_Spp[2], sizeM, cudaMemcpyHostToDevice));
    XXX_profile(load_link);
  }

  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(d_bck2[s], h_bck2[s], sizeV, 
                         cudaMemcpyHostToDevice));
  }
  XXX_profile(load_fermion);

  XXX_profile(cuda_kernel);
  TwoHopDiag<<<Nblock,Nthread>>>( 2,
                                 d_push2[0], d_push2[1], 
                                 d_push2[2], d_push2[3], 
                                 d_pull2[0], d_pull2[1], 
                                 d_pull2[2], d_pull2[3], 
                                 d_Dpp[2], d_Spp[2],  
                                 d_fwd2[0], d_fwd2[1], d_fwd2[2], d_fwd2[3], 
                                 d_bck2[0], d_bck2[1], d_bck2[2], d_bck2[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);
  
  if( load_precalculation ){ 
    XXX_profile(load_link);
    CheckCUDA(cudaMemcpy(d_Spm[0], h_Spm[0], sizeM, cudaMemcpyHostToDevice));
    CheckCUDA(cudaMemcpy(d_Spm[1], h_Spm[1], sizeM, cudaMemcpyHostToDevice));
    XXX_profile(load_link);
  }

  XXX_profile(cuda_kernel);
  TwoHopOffDiag<<<Nblock,Nthread>>>( 1,
                                 d_push1[0], d_push1[1], 
                                 d_push1[2], d_push1[3], 
                                 d_pull0[0], d_pull0[1], 
                                 d_pull0[2], d_pull0[3], 
                                 d_Spm[0], d_Spm[1],  
                                 d_fwd0[0], d_fwd0[1], d_fwd0[2], d_fwd0[3], 
                                 d_bck1[0], d_bck1[1], d_bck1[2], d_bck1[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);
  
  if( load_precalculation ){ 
    XXX_profile(load_link);
    CheckCUDA(cudaMemcpy(d_Spm[2], h_Spm[2], sizeM, cudaMemcpyHostToDevice));
    XXX_profile(load_link);
  }

  XXX_profile(cuda_kernel);
  TwoHopOffDiag<<<Nblock,Nthread>>>( 2,
                                 d_push2[0], d_push2[1], 
                                 d_push2[2], d_push2[3], 
                                 d_pull0[0], d_pull0[1], 
                                 d_pull0[2], d_pull0[3], 
                                 d_Spm[2], NULL,  
                                 d_fwd0[0], d_fwd0[1], d_fwd0[2], d_fwd0[3], 
                                 d_bck2[0], d_bck2[1], d_bck2[2], d_bck2[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);
  
  if( load_precalculation ){ 
    XXX_profile(load_link);
    CheckCUDA(cudaMemcpy(d_Spm[3], h_Spm[3], sizeM, cudaMemcpyHostToDevice));
    CheckCUDA(cudaMemcpy(d_Spm[4], h_Spm[4], sizeM, cudaMemcpyHostToDevice));
    XXX_profile(load_link);
  }

  XXX_profile(cuda_kernel);
  TwoHopOffDiag<<<Nblock,Nthread>>>( 3,
                                 d_push2[0], d_push2[1], 
                                 d_push2[2], d_push2[3], 
                                 d_pull1[0], d_pull1[1], 
                                 d_pull1[2], d_pull1[3], 
                                 d_Spm[3], d_Spm[4],  
                                 d_fwd1[0], d_fwd1[1], d_fwd1[2], d_fwd1[3], 
                                 d_bck2[0], d_bck2[1], d_bck2[2], d_bck2[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);
  
  if( load_precalculation ){ 
    XXX_profile(load_link);
    CheckCUDA(cudaMemcpy(d_Spm[5], h_Spm[5], sizeM, cudaMemcpyHostToDevice));
    CheckCUDA(cudaMemcpy(d_Spm[6], h_Spm[6], sizeM, cudaMemcpyHostToDevice));
    XXX_profile(load_link);
  }

  XXX_profile(cuda_kernel);
  TwoHopOffDiag<<<Nblock,Nthread>>>( 4,
                                 d_pull1[0], d_pull1[1], 
                                 d_pull1[2], d_pull1[3], 
                                 d_pull0[0], d_pull0[1], 
                                 d_pull0[2], d_pull0[3], 
                                 d_Spm[5], d_Spm[6],  
                                 d_fwd0[0], d_fwd0[1], d_fwd0[2], d_fwd0[3], 
                                 d_fwd1[0], d_fwd1[1], d_fwd1[2], d_fwd1[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);
  
  if( load_precalculation ){ 
    XXX_profile(load_link);
    CheckCUDA(cudaMemcpy(d_Spm[7], h_Spm[7], sizeM, cudaMemcpyHostToDevice));
    XXX_profile(load_link);
  }

  XXX_profile(cuda_kernel);
  TwoHopOffDiag<<<Nblock,Nthread>>>( 5,
                                 d_pull2[0], d_pull2[1], 
                                 d_pull2[2], d_pull2[3], 
                                 d_pull0[0], d_pull0[1], 
                                 d_pull0[2], d_pull0[3], 
                                 d_Spm[7], NULL,  
                                 d_fwd0[0], d_fwd0[1], d_fwd0[2], d_fwd0[3], 
                                 d_fwd2[0], d_fwd2[1], d_fwd2[2], d_fwd2[3]);
  cudaThreadSynchronize(); //profiling barrier
  XXX_profile(cuda_kernel);
  
  if( load_precalculation ){ 
    XXX_profile(load_link);
    CheckCUDA(cudaMemcpy(d_Spm[8], h_Spm[8], sizeM, cudaMemcpyHostToDevice));
    CheckCUDA(cudaMemcpy(d_Spm[9], h_Spm[9], sizeM, cudaMemcpyHostToDevice));
    XXX_profile(load_link);
  }

  XXX_profile(cuda_kernel);
  TwoHopOffDiag<<<Nblock,Nthread>>>( 6,
                                 d_pull2[0], d_pull2[1], 
                                 d_pull2[2], d_pull2[3], 
                                 d_pull1[0], d_pull1[1], 
                                 d_pull1[2], d_pull1[3], 
                                 d_Spm[8], d_Spm[9],  
                                 d_fwd1[0], d_fwd1[1], d_fwd1[2], d_fwd1[3], 
                                 d_fwd2[0], d_fwd2[1], d_fwd2[2], d_fwd2[3]);

  cudaThreadSynchronize(); //Do not comment out
  XXX_profile(cuda_kernel);
  
  XXX_profile(load_fermion);
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(h_push0[s], d_push0[s], sizeV, 
                         cudaMemcpyDeviceToHost));
    CheckCUDA(cudaMemcpy(h_pull0[s], d_pull0[s], sizeV, 
                         cudaMemcpyDeviceToHost));
  }
  
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(h_push1[s], d_push1[s], sizeV, 
                         cudaMemcpyDeviceToHost));
    CheckCUDA(cudaMemcpy(h_pull1[s], d_pull1[s], sizeV, 
                         cudaMemcpyDeviceToHost));
  }
  
  #pragma unroll
  for( s = 0; s < 4; s++ ){
    CheckCUDA(cudaMemcpy(h_push2[s], d_push2[s], sizeV, 
                         cudaMemcpyDeviceToHost));
    CheckCUDA(cudaMemcpy(h_pull2[s], d_pull2[s], sizeV, 
                         cudaMemcpyDeviceToHost));
  }
  XXX_profile(load_fermion);
  
  //XXX_profile(cuda_kernel);
#if 0
    //printf( "okaction_dslash_kernel_cuda(): node = %d, device = %d\n", 
    //        this_node, this_device );
    printf( "okaction_dslash_kernel_cuda(): node = %d\n", 
            arrange );
    
    if( arrange == 0 ){

      for( idx = 6*(cuda_volume-1); idx < 6*cuda_volume; idx++ ){
        for( s = 0; s < 4; s++ ){
          printf( "idx = %d, h_pull3[%d] = %le\n", 
                  idx, s, *(h_pull3[s]+idx) );
          
          printf( "idx = %d, h_ans[%d] = %le\n",
                  idx, s, *(h_ans[s]+idx) );
        }
      }

    }
#endif

  //==========
  // All Done
  //==========
  //cudaThreadSynchronize();
  //cudaDeviceSynchronize();
}
/*

*/
