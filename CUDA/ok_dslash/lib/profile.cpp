#include "xxx_profile.h"
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

int XXX_profile(struct xxx_profile *pf)
{
  struct timeval stamp;
  long int sec, usec;
  
  gettimeofday(&stamp,NULL);

  if( pf->addtime ){
    // update the elapsed time
    sec  = stamp.tv_sec - pf->last_stamp.tv_sec;
    usec = stamp.tv_usec - pf->last_stamp.tv_usec;
    pf->elapsed += ((double)( usec + sec*1000000 ))*1.e-6;
    // turn off the flag addtime
    pf->addtime = 0;
  }else{
    // update the time stamp
    pf->last_stamp.tv_sec = stamp.tv_sec;
    pf->last_stamp.tv_usec = stamp.tv_usec;
    // turn on the flag addtime
    pf->addtime = 1;
  }

  // DEBUG
  //fprintf(stdout,"profiling\n");
  //fflush(stdout);

  return 0;
}

int XXX_profile_init(struct xxx_profile *pf)
{
  pf->elapsed = 0.;
  pf->addtime = 0;

  return 0;
}

#ifdef __cplusplus
}
#endif
